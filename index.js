var map = L.map('map', {
  maxBounds: [[46.511, 6.554], [46.541, 6.606]],
  minZoom: 8,
  maxZoom: 18,
  center: [47.0412, 8.126],
  zoom: 16,
})

var stCouleurs = L.tileLayer(
  'https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-farbe/default/current/3857/{z}/{x}/{y}.jpeg',
  { attribution: 'Données &copy; <a href="https://www.swisstopo.admin.ch">Swisstopo</a>' }
)
var stGris = L.tileLayer(
  'https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.pixelkarte-grau/default/current/3857/{z}/{x}/{y}.jpeg',
  { attribution: 'Données &copy; <a href="https://www.swisstopo.admin.ch">Swisstopo</a>' }
)
var stImages = L.tileLayer(
  'https://wmts.geo.admin.ch/1.0.0/ch.swisstopo.swissimage/default/current/3857/{z}/{x}/{y}.jpeg',
  { attribution: 'Données &copy; <a href="https://www.swisstopo.admin.ch">Swisstopo</a>' }
)

var osmLayer = L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://openstreetmap.org">OpenStreetMap</a> contributors'
})

stCouleurs.addTo(map);


var baseLayers = {
  "Carte couleurs": stCouleurs,
  "Carte noir/blanc": stGris,
  "OpenStreetMap": osmLayer,
  "Photos aériennes": stImages,
}
var overlays = {}
L.control.layers(baseLayers, overlays).addTo(map);
