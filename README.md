# Cartes Swisstopo avec Leaflet

Depuis mars 2021, les données Swisstopo sont disponiblement gratuitement. Ainsi, nous pouvons désormais aussi utiliser les fonds Swisswtopo directement dans Leaflet sans devoir payer une compensation.

Swisstopo propose différentes couches sous forme de service WMTS. C'est ce que Leaflet utilise pour afficher les couches de base (L.TileLayer), avec une projection Web Mercator (code EPSG 3857). Swisstopo propose les couches WMTS en plusieurs systèmes de référence spatiale, dont EPSG 3857.

Cet exemple illustre une utilisation très simple des cartes et images Swisstopo. Une liste complète des couches WMTS en EPSG 3857 est disponible ici: [https://wmts.geo.admin.ch/EPSG/3857/1.0.0/WMTSCapabilities.xml](https://wmts.geo.admin.ch/EPSG/3857/1.0.0/WMTSCapabilities.xml).

Bien évidemment, Swisstopo propose ces couches aussi en CH1903+ (EPSG 2056).
